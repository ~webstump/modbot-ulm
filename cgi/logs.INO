#!/bin/bash
set -e
cd %BASEDIR%/xlog/log
exec ../bin/report %GROUP% %STATICFILESURL%/g.%ABBREV%/messages "$@"
